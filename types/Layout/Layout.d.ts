/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface LayoutProps extends Props {
}

export default class Layout extends SvelteComponentTyped<LayoutProps, {}, {'default':{},'left':{},'right':{}}> {

}
