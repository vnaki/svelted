/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface BreadcrumbItemProps extends Props {
}

export default class BreadcrumbItem extends SvelteComponentTyped<BreadcrumbItemProps, {
    click: CustomEvent<any>;
}, {default:{}, separator:{}}> {
}
