/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface HeaderProps extends Props {
}

export default class Header extends SvelteComponentTyped<HeaderProps, {}, {'default':{},}> {

}
