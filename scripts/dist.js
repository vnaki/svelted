import { defineConfig } from 'rollup';
import terser from '@rollup/plugin-terser';
import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import svelte from 'rollup-plugin-svelte';
import sveltePreprocess from 'svelte-preprocess';
import pkg from '../package.json' assert { type: 'json' };

export default defineConfig({
    input: 'packages/index.ts',
    external: [
        'svelte-design-icons'
    ],
    output: [
        {
            format: 'umd',
            file: pkg.main,
            name: pkg.name,
            globals: {
                'svelte-design-icons': 'svelte-design-icons'
            },
        },
        {
            format: 'es',
            file: pkg.module,
        }
    ],
    plugins: [
        svelte({
            preprocess: sveltePreprocess(),
        }),
        resolve({
            dedupe: ['svelte'],
        }),
        typescript(),
        terser(),
    ]
});