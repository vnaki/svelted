
export function prefix(name:string) {
    return `svelted-${name}`;
}

export function classname(rest: (string | { [key: string]: boolean })[], extra:string = ''): string {
    let n = '';

    const concat = (v: string) => {
        if (n !== '') {
            n += ' ';
        }
        n += prefix(v);
    };

    rest.forEach(function (v) {
        if (typeof v === 'string') {
            if (v !== '') {
                concat(v);
            }
        } else {
            for (let k in v) {
                if (k !== '' && (v as Object).hasOwnProperty(k) && v[k]) {
                    concat(k);
                }
            }
        }
    });

    return extra !== '' ? `${n} ${extra}` : n;
}
