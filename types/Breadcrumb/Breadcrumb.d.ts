/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface BreadcrumbProps extends Props {
}

export default class Breadcrumb extends SvelteComponentTyped<BreadcrumbProps, {}, {default:{}}> {
}
