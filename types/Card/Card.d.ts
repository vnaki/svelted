/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface CardProps extends Props {
    /**
     * 是否划过阴影效果
     * @default false
     */ 
    shadow?:boolean;
}

export default class Card extends SvelteComponentTyped<CardProps, {}, {
    'default':{},
    'header':{},
    'footer':{}}> {
}
