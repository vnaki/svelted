import sass from 'sass';
import cssnano from 'cssnano';
import postcss from 'postcss';
import autoprefixer from 'autoprefixer';
import fs from 'fs';

const theme = 'packages/theme';
const { css } = await sass.compileAsync(`${theme}/index.scss`);
const plugins = [
    cssnano(),
    autoprefixer({
        overrideBrowserslist: ['>0.2%', 'not dead', 'ie >= 11', 'last 2 versions'],
    }),
];
const output = await postcss(plugins).process(css, {from:undefined});
const dist = 'style';

// write compiled css
fs.mkdirSync(dist, {recursive:true});
fs.writeFileSync(`${dist}/index.css`, output.css, {flag: 'w+'});

// copy scss files
fs.cpSync(theme, `${dist}/theme`, {force:true, recursive:true});

console.log('\x1B[36m%s\x1B[0m', `${theme}/index.scss → ${dist}/index.css, size: ${(output.css.length/1024).toFixed(2)}kb`);