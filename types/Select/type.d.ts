
export type SelectOption = {
    label:string,
    value:string|number,
    key:string|number,
    disabled?:boolean,
};
