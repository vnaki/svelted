
export function once(event: string, callback: { (e:Event):boolean }) {
    function listener(e:Event) {
        // true: remove listener
        if (callback(e)) {
            document.removeEventListener(event, listener);
        }
    }
    document.addEventListener(event, listener);
}

export function event(remove:boolean, event:string, callback:{(e:Event):void}) {
    document[remove?'removeEventListener':'addEventListener'](event, callback);
}