
export function parseNumber(value:string) {
    if(value[0] === '-') {
        if(value.length > 1) {
            let pv = parseFloat(value);
            value = isNaN(pv) ? '-' : `${pv}`;
        }
    } else {
        let pv = parseFloat(value);
        value = isNaN(pv) ? '' : `${pv}`;
    }

    return value;
}

export function parseRange(value:number|string, min:number, max:number) {
    if(typeof value === 'string') {
        value = parseFloat(value);

        if(isNaN(value)) {
            return '';
        }
    }
    if(min && value <= min) {
        return min;
    }
    if(max && value >= max) {
        return max;
    }
    return value;
}
