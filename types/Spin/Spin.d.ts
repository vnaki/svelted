/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface SpinProps extends Props {
    /**
     * 尺寸, 默认16
     * @default 16
     */ 
    size?:number;

    /**
     * 是否旋转
     * @default false
     */ 
    spin?:boolean;

    /**
     * 遮罩层`class`
     * @default undefined
     */ 
    mask?:string;
}

export default class Spin extends SvelteComponentTyped<SpinProps, {}, {default:{}}> {
}
