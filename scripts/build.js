import { exec } from 'child_process';

[
    'npm run build:css',
    'npm run build:dist',
].forEach(cmd => exec(cmd, (err, stdout, stderr) => {
    if(err) {
        console.error(err);
    } else {
        console.log(stdout, stderr);
    }
}));
