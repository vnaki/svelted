/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface RadioProps extends Props {
    /**
     * 选项值
     * @required
     */
    value:string|number;

    /**
     * 是否禁用
     * @default false
     */
    disabled?:boolean;
}

export default class Radio extends SvelteComponentTyped<RadioProps, {
    focus: CustomEvent<{}>;
    blur: CustomEvent<{}>;
}, {
    default:{},
    radio:{},
}> {}
