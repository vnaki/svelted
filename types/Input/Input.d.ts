/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface InputProps extends Props {
    /**
     * @default text
     */ 
    type?:'text'|'password';

    /**
     * @default default
     */ 
    size?:'small'|'large'|'default';

    /**
     * @default undefined
     */ 
    name?:string;

    /**
     * @default ''
     */ 
    value?:string|number;

    /**
     * @default undefined
     */ 
    placeholder?:string;

    /**
     * @default 0
     */ 
    length?:number;

    /**
     * @default false
     */ 
    readonly?:boolean;

    /**
     * @default false
     */ 
    disabled?:boolean;

    /**
     * @default false
     */ 
    autofocus?:boolean;
}

export default class Input extends SvelteComponentTyped<InputProps, {
    input: CustomEvent<{value:any}>,
    change: CustomEvent<{value:any}>,
    focus: CustomEvent<{value:any}>,
    blur: CustomEvent<{value:any}>,
    mouseenter: CustomEvent<{value:any}>,
    mouseleave: CustomEvent<{value:any}>,
}, {'prefix':{},'suffix':{}}> {

}
