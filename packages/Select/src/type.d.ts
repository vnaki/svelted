
export type State = {(value:string|number):void};

export type Context = {
    value:string|number, 
    set:{(label:string, value:string|number, state:{():void}):void},
    state:{(f:State):void},
};

export type SelectOption = {
    label:string,
    value:string|number,
    key:string|number,
    disabled?:boolean,
};