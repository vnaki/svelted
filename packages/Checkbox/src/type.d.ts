
export type Change = {(value:string|number, checked:boolean)};
export type Limited = {():boolean};
export type CheckboxContext = {change:Change, limited:Limited};
