/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';

export interface RadioGroupProps {
    /**
     * 名称
     * @required
     */
    name:string;

    /**
     * 默认值
     * @default undefined
     */
    value?:string|number;
}

export default class RadioGroup extends SvelteComponentTyped<RadioGroupProps, {
    change: CustomEvent<{name:string, value:string|number}>;
}, {'default':{}}> {}
