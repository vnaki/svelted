/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { IconType } from 'svelte-design-icons/types';

export interface IconProps {
    /**
     * @required
     */ 
    icon:IconType;

    /**
     * @default 16
     */ 
    size?:number;

    /**
     * @default undefined
     */ 
    color?:string;

    /**
     * @default undefined
     */ 
    stroke?:string;

    /**
     * @default undefined
     */ 
    fill?:string;

    /**
     * @default 3
     */ 
    strokeWidth?:number;

    /**
     * @default round
     */ 
    strokeLinecap?:'butt'|'round'|'square';

    /**
     * @default round
     */ 
    strokeLinejoin?:'miter'|'round'|'bevel';

    /**
     * @default false
     */ 
    spin?:boolean;

    /**
     * @default undefined
     */
    class?:string;
}

export default class Icon extends SvelteComponentTyped<IconProps, {}, {}> {

}
