/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';

export interface CheckboxGroupProps {
    /**
     * 名称
     * @required
     */
    name:string;

    /**
     * 数量限制, 小于1不限制
     * @default 0
     */
    limit?:number;
}

export default class CheckboxGroup extends SvelteComponentTyped<CheckboxGroupProps, {
    limit: CustomEvent<{limit:number}>;
    change: CustomEvent<{name:string, value:Array<string|number>}>;
}, {'default':{}}> {}
