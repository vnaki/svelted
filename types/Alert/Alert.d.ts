/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface AlertProps extends Props {
    /**
     * 警告类型
     * @default primary
     */
    type?:'primary'|'success'|'warning'|'danger';

    /**
     * 是否可关闭
     * @default false
     */
    closable?:boolean;
}

declare class Alert extends SvelteComponentTyped<AlertProps, {
    click: CustomEvent<any>;
}, {default:{}, title:{}}> {

}

export default Alert;
