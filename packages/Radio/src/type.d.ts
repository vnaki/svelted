export type RadioContext = {name:string, value:string|number, change:{(v:string|number, state:{():void}):void}};
