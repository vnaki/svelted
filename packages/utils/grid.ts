import type { Gutter, MediaGutter, Offset, MediaOffset, Span } from '../Grid/src/type';

const medias = ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'];

export function getRowGutterClass(gutter:Gutter|MediaGutter) {
    if(typeof gutter === 'number') {
        return [`row-gutter-${gutter}`];
    }

    let gutterClass = [];

    gutter && medias.forEach(function(media) {
        if(gutter[media]) {
            gutterClass.push(`row-${media}-gutter-${gutter[media]}`);
        }
    });

    return gutterClass;
}

export function getColOffsetClass(offset:Offset|MediaOffset) {
    if(typeof offset === 'number' && offset !== 0) {
        return [`col-offset-${offset}`];
    }

    let offsetClass = [];

    offset && medias.forEach(function(media) {
        if(offset[media] && offset[media] !== 0) {
            offsetClass.push(`col-${media}-gutter-${offset[media]}`);
        }
    });

    return offsetClass;
}

export function getColSpanClass(span:{[prop:string]:Span}) {
    let spanClass = ['col', `col-${span['span']}`];

    span && medias.forEach(function(media) {
        if(span[media]) {
            spanClass.push(`col-${media}-${span[media]}`);
        }
    });

    return spanClass;
}
