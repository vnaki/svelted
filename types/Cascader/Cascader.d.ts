/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

type CascaderOption = {
    label:string;
    value:string|number;
    key:string;
    disabled?:boolean;
    options?:CascaderOption[];
};

export interface CascaderProps extends Props {
    /**
     * @required
     */
    name:string;

    /**
     * 默认值
     * @default []
     */
    value?:Array<string|number>;

    /**
     * 尺寸, 可选 small、large
     * @default default
     */ 
    size?:'small'|'large'|'default';

    /**
     * 下拉选项数据
     * @required
     */ 
    options:CascaderOption[];

    /**
     * @default undefined
     */ 
    placeholder?:string;

    /**
     * @default false
     */ 
    disabled?:boolean;

    /**
     * 是否可搜索
     * 
     * @default false
     */ 
    searchable?:boolean;

    /**
     * 是否加载中
     * @default false
     */ 
    loading?:boolean;

    /**
     * 下拉选项是否朝上
     * @default false
     */ 
    up?:boolean;
}

export default class Cascader extends SvelteComponentTyped<CascaderProps, {
    focus: CustomEvent<{}>;
    blur: CustomEvent<{}>;
    change: CustomEvent<{name:string, value:string|number}>;
}, {}> {}
