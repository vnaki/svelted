/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Gutter, MediaGutter } from './type';
import type { Props } from '../base/props';

export interface RowProps extends Props {
    /**
     * @default false
     */ 
    wrap?:boolean;

    /**
     * @default undefined
     */ 
    gutter?:Gutter|MediaGutter;
}

export default class Row extends SvelteComponentTyped<RowProps, {}, {default:{}}> {

}
