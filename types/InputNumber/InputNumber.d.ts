/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface InputNumberProps extends Props {
    /**
     * @default undefined
     */ 
    min?:number;

    /**
     * @default undefined
     */ 
    max?:number;
    
    /**
     * @default 1
     */ 
    step?:number;

    /**
     * @default default
     */ 
    size?:'small'|'large'|'default';
    
    /**
     * @default undefined
     */ 
    name?:string;

    /**
     * @default ''
     */ 
    value?:string|number;
 
    /**
     * @default undefined
     */ 
    placeholder?:string;

    /**
     * @default false
     */ 
    readonly?:boolean;

    /**
     * @default false
     */ 
    disabled?:boolean;

    /**
     * @default false
     */ 
    autofocus?:boolean;
}

export default class InputNumber extends SvelteComponentTyped<InputNumberProps, {
    input: CustomEvent<{value:any}>,
    change: CustomEvent<{value:any}>,
    focus: CustomEvent<{value:any}>,
    blur: CustomEvent<{value:any}>,
    mouseenter: CustomEvent<{value:any}>,
    mouseleave: CustomEvent<{value:any}>,
}, {'prefix':{},'suffix':{}}> {

}
