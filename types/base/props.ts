
export interface Props {
    /**
     * @default undefined
     */
    class?:string;

    /**
     * @default undefined
     */
    style?:string;

    /**
     * @default undefined
     */
    id?:string;

    /**
     * @default undefined
     */
    slot?:string
}
