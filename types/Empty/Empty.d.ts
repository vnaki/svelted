/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface EmptyProps extends Props {
    /**
     * 图片尺寸,默认66
     * @default 66
     */ 
    size?:number;
}

declare class Empty extends SvelteComponentTyped<EmptyProps, {}, {
    default:{},
    image:{},
}> {
    
}

export default Empty;
