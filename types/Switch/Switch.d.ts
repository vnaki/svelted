/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface SwitchProps extends Props {
    /**
     * 名称
     * @required
     */
    name:string;

    /**
     * 默认值, 必须等于onValue或offValue
     * @required
     */
    value:string|number;

    /**
     * 尺寸
     * @default default
     */
    size?:'small'|'default';

    /**
     * 是否禁用
     * @default false
     */
    disabled?:boolean;

    /**
     * 是否加载中
     * @default false
     */
    loading?:boolean;

    /**
     * 开 - 文案
     * @required
     */
    onText:string;

    /**
     * 开 - 值
     * @required
     */
    onValue:string|number;

    /**
     * 关 - 文案
     * @required
     */
    offText:string;

    /**
     * 关 - 值
     * @required
     */
    offValue:string|number;
}

export default class Switch extends SvelteComponentTyped<SwitchProps, {
    change: CustomEvent<{name:string, value:string|number}>;
}, {
    default:{},
}> {}
