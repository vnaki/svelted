> Please note: This library is in active development.

### A UI library based on Svelte

> visit: https://svelte.design

##### 1. Install

- install svelte-design

```
npm i svelte-design
```

- install svelte-design-icons

```
npm i svelte-design-icons
```

##### 2. SCSS Usage

set font-family

```
@import 'svelte-design/style/theme/index.scss';

* {
    font-family: $font-family;
}
```

use scss variables

```
@import 'svelte-design/style/theme/utils/vars.scss';
```

#### Component List

| 组件 | 名称 | 开发状态(Y/N) |
| --- | --- | --- |
| Alert | 警告提示 | Y |
| Breadcrumb | 面包屑 | Y |
| Button | 按钮 | Y |
| Card | 卡片 | Y |
| Grid | 栅格 | Y |
| Icon | 图标 | Y |
| Input | 文本输入框 | Y |
| InputNumber | 数字输入框 | Y |
| Layout | 布局 | Y |
| Select | 下拉选项 | Y |
| Spin | 旋转 | Y |