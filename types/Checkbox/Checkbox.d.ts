/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface CheckboxProps extends Props {
    /**
     * 选项值
     * @default undefined
     */
    value?:string|number;

    /**
     * 是否选中
     * @default false
     */
    checked?:boolean;

    /**
     * 是否禁用
     * @default false
     */
    disabled?:boolean;

    /**
     * 是否不确定状态
     * @default false
     */
    indeterminate?:boolean;
}

export default class Checkbox extends SvelteComponentTyped<CheckboxProps, {
    focus: CustomEvent<{}>;
    blur: CustomEvent<{}>;
    change: CustomEvent<{value:string|number, checked:boolean}>;
}, {
    default:{},
    checkbox:{}
}> {}
