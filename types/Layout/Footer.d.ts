/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface FooterProps extends Props {
}

export default class Footer extends SvelteComponentTyped<FooterProps, {}, {'default':{},}> {

}
