export type State = {(value:string|number):void};

export type CascaderOption = {
    label:string;
    value:string|number;
    key:string;
    disabled?:boolean;
    options?:CascaderOption[];
};

export type Context = {
    value:string|number, 
    set:{(label:string, value:string|number, state:{():void}):void},
    state:{(s:State):void},
};
