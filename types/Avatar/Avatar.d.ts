/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface AvatarProps extends Props {
    /**
     * 图片地址
     * @default undefined
     */
    src?:string;

    /**
     * 图片Alt
     * @default avatar
     */
    alt?:string;

    /**
     * 尺寸
     * @default default
     */
    size?:'large'|'small'|'default';

    /**
     * 是否圆形
     * @default false
     */
    circle?:boolean;
}

export default class Avatar extends SvelteComponentTyped<AvatarProps, {
    error:CustomEvent<any>,
}, {
    default:{},
}> {

}
