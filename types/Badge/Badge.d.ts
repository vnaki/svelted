/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface BadgeProps extends Props {
    /**
     * 是否圆点
     * @default false
     */
    dot?:boolean;

    /**
     * 徽章类型
     * @default danger
     */
    type?:'primary'|'success'|'warning'|'danger';
}

export default class Badge extends SvelteComponentTyped<BadgeProps, {}, {default:{},badge:{}}> {

}
