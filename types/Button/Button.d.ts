/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { IconType } from 'svelte-design-icons/types';
import type { Props } from '../base/props';

export interface ButtonProps extends Props {
    /**
     * 按钮图标
     * @default undefined
     */
    icon?:IconType;

    /**
     * 是否加载状态
     * @default false
     */
    loading?:boolean;

    /**
     * 尺寸
     * @default default
     */
    size?:'small'|'large'|'default';

    /**
     * 按钮风格类型
     * @default default
     */
    type?:'primary'|'outline'|'default';

    /**
     * @default button
     */   
    htmlType?:'submit'|'button'|'reset';

    /**
     * 是否禁用
     * @default false
     */   
    disabled?:boolean;
}

export default class Button extends SvelteComponentTyped<ButtonProps, {
    click: CustomEvent<any>;
}, {default:{}}> {

}
