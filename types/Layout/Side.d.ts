/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface SideProps extends Props {
}

export default class Side extends SvelteComponentTyped<SideProps, {}, {'default':{},}> {
}
