/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface ContentProps extends Props {
}

export default class Content extends SvelteComponentTyped<ContentProps, {}, {'default':{},}> {

}
