
export type Gutter = 1|2|3|4|5|6|7|8|9|10;
export type MediaGutter = {
    xs?:Gutter,
    sm?:Gutter,
    md?:Gutter,
    lg?:Gutter,
    xl?:Gutter,
    xxl?:Gutter,
};
export type Span = 0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24;
export type Offset = Span;
export type MediaOffset = {
    xs?:Offset,
    sm?:Offset,
    md?:Offset,
    lg?:Offset,
    xl?:Offset,
    xxl?:Offset,
};
