/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';
import type { SelectOption } from './type';

export interface SelectProps extends Props {
    /**
     * @required
     */ 
    name:string;

    /**
     * @default value
     */ 
    value?:string|number;
    
    /**
     * 尺寸, 可选 small、large
     * @default default
     */ 
    size?:'small'|'large'|'default';

    /**
     * 下拉选项数据
     * @required
     */ 
    options:SelectOption[];

    /**
     * @default undefined
     */ 
    placeholder?:string;

    /**
     * @default false
     */ 
    disabled?:boolean;

    /**
     * 是否可搜索
     * 
     * @default false
     */ 
    searchable?:boolean;

    /**
     * 是否加载中
     * @default false
     */ 
    loading?:boolean;

    /**
     * 下拉选项是否朝上
     * @default false
     */ 
    up?:boolean;
}

export default class Select extends SvelteComponentTyped<SelectProps, {
    focus: CustomEvent<{}>,
    blur: CustomEvent<{}>,
    input: CustomEvent<{name:string, value:any}>,
    change: CustomEvent<{name:string, value:any}>,
}, {default:{},prefix:{},empty:{}}> {

}
