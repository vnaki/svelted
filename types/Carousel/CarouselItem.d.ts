/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface CarouselItemProps extends Props {
}

export default class CarouselItem extends SvelteComponentTyped<CarouselItemProps, {}, {'default':{}}> {
}
