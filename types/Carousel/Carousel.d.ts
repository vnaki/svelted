/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Props } from '../base/props';

export interface CarouselProps extends Props {
    /**
     * 默认播放索引位置
     * @default 0
     */
    index?:number;

    /**
     * 播放动画时长, 单位毫秒
     * @default 800
     */
    duration?:number;

    /**
     * 播放动画间隔, 单位毫秒
     * @default 5000
     */
    interval?:number;

    /**
     * 是否自动播放
     * @default true
     */
    autoplay?:boolean;

    /**
     * 是否显示指示器
     * @default true
     */
    showIndicator?:boolean;

    /**
     * 是否垂直方向, true:垂直方向, false:水平方向
     * @default true
     */
    vertical?:boolean;

    /**
     * 动画类型
     * @default ease-in-out
     */
    animation?:'ease'|'ease-in'|'ease-out'|'ease-in-out'|'linear';
}

export default class Carousel extends SvelteComponentTyped<CarouselProps, {
    play: CustomEvent<{index:number}>;
}, {'default':{}}> {}
