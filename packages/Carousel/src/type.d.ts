
export type CarouselState = {(
    from:string,
    to:string, 
    duration:number,
    // 是否主场
    stage:boolean,
):void};

export type CarouselAnimation = 'ease'|'ease-in'|'ease-out'|'ease-in-out'|'linear';

export type CarouselContext = {
    item:{(state:CarouselState):boolean},
    stop:{():void},
    vertical:boolean,
    animation:CarouselAnimation,
};

export type CarouselFrameAction = 'prev' | 'next';
