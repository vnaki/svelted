/// <reference types='svelte' />

import type { SvelteComponentTyped } from 'svelte';
import type { Offset, MediaOffset, Span } from './type';
import type { Props } from '../base/props';

export interface ColProps extends Props {
    /**
     * 栅格数
     * @default 24
     */ 
    span?:Span;

    /**
     * 栅格数
     * @default undefined
     */ 
    xs?:Span;

    /**
     * 栅格数
     * @default undefined
     */ 
    sm?:Span;

    /**
     * 栅格数
     * @default undefined
     */ 
    md?:Span;

    /**
     * 栅格数
     * @default undefined
     */ 
    lg?:Span;

    /**
     * 栅格数
     * @default undefined
     */ 
    xl?:Span;

    /**
     * 栅格数
     * @default undefined
     */ 
    xxl?:Span;

    /**
     * 偏移量
     * @default undefined
     */ 
    offset?:Offset|MediaOffset;

    /**
     * 排序
     * @default undefined
     */ 
    order?:number;
}

export default class Col extends SvelteComponentTyped<ColProps, {}, {default:{}}> {

}
